#!/bin/ash

# take ownership of workspace
sudo chown -R `whoami` /lxccm

# install pre-downloaded packages
find /lxccm/*tcz& | xargs tce-load -i

# configure keys & start ssh
sudo mv /usr/local/etc/ssh/sshd_config.orig /usr/local/etc/ssh/sshd_config
sudo mv /lxccm/keypack/miner/host/* /usr/local/etc/ssh/
mkdir $HOME/.ssh
mv /lxccm/keypack/miner/server.pub $HOME/.ssh/authorized_keys
mv /lxccm/keypack/miner/ecdsa $HOME/.ssh/known_hosts
chmod 600 /lxccm/keypack/miner/client
sudo /usr/local/etc/init.d/openssh start

# get miner & config bundle
scp -P 5001 -i /lxccm/keypack/miner/client lxccm@10.127.3.1:/lxccm/bundle.tar.gz /lxccm/bundle.tar.gz
tar zxf /lxccm/bundle.tar.gz -C /lxccm/

# build and install hwloc
cd /lxccm/hwloc
./configure --prefix=/usr/local
make
sudo make install

# build xmr-stak
cd /lxccm/miner
mkdir build && cd build
CC=gcc cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF -DMICROHTTPD_ENABLE=OFF
make install

# move config files and start mining
cd bin
mv /lxccm/config/* .
./xmr-stak
