#!/bin/ash

# run this while using a (preferably clean) corepure64/tinycorepure64 environment
# this is an automated script to build a minimal tinycore flavor for my very personal/spesific mining needs

# get keypack link
read -p "Enter keypack.tar.gz link: " kplink

# make sure workspace is fresh
rm -rf /tmp/core_extract/ /tmp/core_package/ /tmp/corepure64.gz /tmp/hwloc 2> /dev/null

# download a new corepure64.gz
until wget http://tinycorelinux.net/9.x/x86_64/release/distribution_files/corepure64.gz -O /tmp/corepure64.gz;
do
    echo "Could not download a stock corepure64.gz, retrying...";
    sleep 2;
done

# download and install required packages
until tce-load -wi core-remaster.tcz \
                   openssl-dev.tcz \
                   cmake.tcz \
                   make.tcz \
                   gcc.tcz \
                   git.tcz \
                   glibc_base-dev.tcz \
                   linux-4.14.3_api_headers.tcz \
                   glibc_add_lib.tcz \
                   flex.tcz &&
    wget https://download.open-mpi.org/release/hwloc/v2.0/hwloc-2.0.2.tar.gz -O /tmp/hwloc &&
    wget $kplink -O /tmp/keypack;
do
    echo "Could not download required packages, retrying...";
    sleep 2;
done

# extract core
cat << EOF | core-remaster

e

EOF

# create a workspace in the extracted core, copy packages, keypack and onboot script
mkdir -p /tmp/core_extract/lxccm/tcz
cp onboot.sh /tmp/core_extract/lxccm/onboot.sh
cp /tmp/tce/optional/* /tmp/core_extract/lxccm/tcz/
find /tmp/core_extract/lxccm/tcz/core-remaster* -exec rm -f {} \;
echo "ash /lxccm/onboot.sh" >> /tmp/core_extract/etc/skel/.profile
tar zxf /tmp/keypack -C /tmp/core_extract/lxccm
tar zxf /tmp/hwloc -C /tmp/core_extract/lxccm && mv /tmp/core_extract/lxccm/hwloc* /tmp/core_extract/lxccm/hwloc

# re-package core
cat << EOF | core-remaster

p

EOF

# cleanup
rm -rf /tmp/keypack.tar.gz /tmp/hwloc
