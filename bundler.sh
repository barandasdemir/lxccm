#!/usr/bin/env bash

# run this script ONLY on the router

# download miner & bundle with config
cd /lxccm/repo
git clone https://github.com/fireice-uk/xmr-stak miner
sed -i "s/#pragma once/#pragma once\n#define _GLIBCXX11_USE_C99_MATH 1/g" miner/xmrstak/misc/executor.hpp
sed -i "s/2.0/0.0/g" miner/xmrstak/donate-level.hpp
tar zcf bundle.tar.gz miner/ config/
